import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_FAQ_LIST]: (store) => {
        return axios.get(apiUrls.DO_FAQ_LIST)
    },
}

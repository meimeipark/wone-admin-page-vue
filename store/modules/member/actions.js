import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_USER_LIST]: (store) => {
        return axios.get(apiUrls.DO_USER_LIST)
    },
    [Constants.DO_USER_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_USER_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST_PAGING.replace('{pageNum}', payload.pageNum) )
    },
}

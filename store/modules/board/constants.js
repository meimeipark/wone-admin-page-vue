export default {
    DO_NOTICE_LIST: 'board/doNoticeList', //get 공지사항만 조회
    DO_GUIDE_LIST: 'board/doGuideList', //get 이용방법만 조회
    DO_NOTICE_DETAIL: 'board/doNoticeDetail',
    DO_CREATE_NOTICE: 'board/doCreateNotice',

}

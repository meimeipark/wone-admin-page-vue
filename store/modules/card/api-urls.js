const BASE_URL = '/v1/card'

export default {
    DO_CARD_LIST: `${BASE_URL}/all/member-id/{memberId}`, //get
    DO_CARD_DETAIL: `${BASE_URL}/detail/{id}`, //get

}

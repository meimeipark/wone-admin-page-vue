import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CategoryType from "@/components/layout/category-type.vue";
Vue.component('CategoryType', CategoryType)
